package fr.mds.designpattern.builder.models;

public abstract class Game implements Item {
	
	@Override
	public float price() {
		return 0;
	}
	
	@Override
	public Packing packing() {
		return new Wrapper();
	}
}
