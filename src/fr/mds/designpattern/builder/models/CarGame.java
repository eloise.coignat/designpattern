package fr.mds.designpattern.builder.models;

public class CarGame extends Game {

	@Override
	public String name() {
		return "Tiens mon enfant une petite voiture !";
	}
	
	@Override
	public String toString() {
		return name();
	}

}
