package fr.mds.designpattern.builder.models;

public class ChickenBurger extends Burger {

	@Override
	public String name() {
		return "Chicken burger";
	}
	
	@Override 
	public float price() {
		return 5;
	}
	
	@Override
	public String toString() {
		return name() + " " + price();
	}
}
