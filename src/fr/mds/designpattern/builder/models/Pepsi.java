package fr.mds.designpattern.builder.models;

public class Pepsi extends ColdDrink {

	@Override
	public String name() {
		return "Pepsi";
	}
	
	@Override 
	public float price() {
		return 3;
	}
	
	@Override
	public String toString() {
		return name() + " " + price();
	}

}
