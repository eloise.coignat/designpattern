package fr.mds.designpattern.builder.models;

public interface Item {
	
	public String name();
	public float price();
	public Packing packing();

}
