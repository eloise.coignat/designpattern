package fr.mds.designpattern.builder.models;

public class VegBurger extends Burger {
	
	@Override
	public String name() {
		return "Veg burger";
	}
	
	@Override 
	public float price() {
		return 12;
	}
	
	@Override
	public String toString() {
		return name() + " " + price();
	}

}
