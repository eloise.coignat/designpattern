package fr.mds.designpattern.builder.models;

import java.util.*;

public class Meal {
	
	private String name;
	private float cost;
	private List<Item> items = new ArrayList<>();
	private Game game;

	public Meal () {

	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getCost() {
		
		for (Item item : items) {
			cost += item.price();
		}
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public void showItems() {
		for (Item item : items) {
			System.out.println(item);
		}
	}
	
	public void addItem (Item item) {
		if (!this.items.contains(item)) {
			items.add(item);
		}	
	}

}
