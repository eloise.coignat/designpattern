package fr.mds.designpattern.builder.models;

public class DrawGame extends Game {
	
	@Override
	public String name() {
		return "Tiens mon enfant un coloriage pour ta patience !";
	}
	
	@Override
	public String toString() {
		return name();
	}

}
