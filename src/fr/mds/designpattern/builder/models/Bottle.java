package fr.mds.designpattern.builder.models;

public class Bottle implements Packing {
	
	private String name;

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Bottle";
	}

}
