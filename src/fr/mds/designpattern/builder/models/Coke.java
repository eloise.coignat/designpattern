package fr.mds.designpattern.builder.models;

public class Coke extends ColdDrink {

	@Override
	public String name() {
		return "Coke";
	}
	
	@Override 
	public float price() {
		return 6;
	}
	
	@Override
	public String toString() {
		return name() + " " + price();
	}
}
