package fr.mds.designpattern.builder.models;

import java.util.*;

public class MealBuilder {
	
	protected List<Item> items = new ArrayList<>();
	
	public MealBuilder prepareVegMeal() {
		items.add(new VegBurger());
		items.add(new Pepsi());
		items.add(new Coke());
		return this;
	}
	
	public MealBuilder prepareNonVegMeal() {
		items.add(new ChickenBurger());
		items.add(new Coke());
		return this;
	}
	
	public MealBuilder prepareKidMealCar() {
		items.add(new CarGame());
		return this;
	}
	
	public MealBuilder prepareKidMealDraw() {
		items.add(new DrawGame());
		return this;
	}
	
	public MealBuilder addItem(Item item) {
		items.add(item);
		return this;
	}
	
	public Meal build() {
		final Meal result = new Meal();

		result.setItems(this.items);
		
		items = new ArrayList<>();
		
		return result;
	}

}
