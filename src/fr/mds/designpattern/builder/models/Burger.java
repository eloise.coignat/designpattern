package fr.mds.designpattern.builder.models;

public abstract class Burger implements Item {
	
	@Override
	public Packing packing() {
		return new Wrapper();
	}

}
