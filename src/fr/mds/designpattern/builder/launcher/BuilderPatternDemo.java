package fr.mds.designpattern.builder.launcher;

import fr.mds.designpattern.builder.models.ChickenBurger;
import fr.mds.designpattern.builder.models.Coke;
import fr.mds.designpattern.builder.models.Meal;
import fr.mds.designpattern.builder.models.MealBuilder;

public class BuilderPatternDemo {

	public static void main(String[] args) {
		
		MealBuilder mealBuilder = new MealBuilder();

		System.out.println("Bonjour, puis-je avoir le menu JE SUIS TOTALEMENT VEGAN, s'il vous pla�t ?");
		Meal vegMeal = mealBuilder.prepareVegMeal().build();		
		vegMeal.showItems();
		System.out.println("Total cost : " + vegMeal.getCost());
		
		System.out.println("\nBonjour, puis-je avoir le menu JE SUIS TOTALEMENT MIAM MIAM ANIMAUX, s'il vous pla�t ?");
		Meal nonVegMeal = mealBuilder.prepareNonVegMeal().build();		
		nonVegMeal.showItems();
		System.out.println("Total cost : " + nonVegMeal.getCost());
		
		System.out.println("\nBonjour, puis-je faire mon menu moi-m�me, s'il vous pla�t ?");
		Meal customMeal = mealBuilder.prepareVegMeal().addItem(new ChickenBurger()).addItem(new Coke()).build();		
		customMeal.showItems();
		System.out.println("Total cost : " + customMeal.getCost());
		
		System.out.println("\nBonjour, puis-je faire un menu HAPPY VEGMEAL avec une voiture, s'il vous pla�t ?");
		Meal kidMealCar = mealBuilder.prepareVegMeal().prepareKidMealCar().addItem(new Coke()).build();		
		kidMealCar.showItems();
		System.out.println("Total cost : " + kidMealCar.getCost());
		
		System.out.println("\nBonjour, puis-je faire mon menu HAPPY MEAL avec un coloriage, s'il vous pla�t ?");
		Meal kidMealDraw = mealBuilder.prepareNonVegMeal().prepareKidMealDraw().addItem(new Coke()).build();		
		kidMealDraw.showItems();
		System.out.println("Total cost : " + kidMealDraw.getCost());
		
		
		
	}
}
