package fr.mds.designpattern.adapter.models;

public interface AdvancedMediaPlayer {
	
	public void playVlc(String fileName);
	public void playMp4(String fileName);
	public void playMp3(String fileName);

}
