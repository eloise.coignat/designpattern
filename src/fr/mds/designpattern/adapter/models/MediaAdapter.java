package fr.mds.designpattern.adapter.models;

public class MediaAdapter implements MediaPlayer {

	AdvancedMediaPlayer advancedMediaPlayer;
		
	public MediaAdapter(String fileType) {
		
		if (fileType.equalsIgnoreCase("mp4")) {
			advancedMediaPlayer = new Mp4Player();
		}
		else if (fileType.equalsIgnoreCase("vlc")) {
			advancedMediaPlayer = new VlcPlayer();
		}
		else if (fileType.equalsIgnoreCase("mp3")) {
			advancedMediaPlayer = new Mp4Player();
			
		}
	}
	
	@Override
	public void play(String fileType, String fileName) {
		
		if (fileType.equalsIgnoreCase("mp4")) {
			advancedMediaPlayer.playMp4(fileName);
		}
		else if (fileType.equalsIgnoreCase("vlc")) {
			advancedMediaPlayer.playVlc(fileName);
		}
		else if (fileType.equalsIgnoreCase("mp3")) {
			advancedMediaPlayer.playMp3(fileName);
		}
	}

}
