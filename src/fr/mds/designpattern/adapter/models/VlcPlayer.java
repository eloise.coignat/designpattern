package fr.mds.designpattern.adapter.models;

public class VlcPlayer implements AdvancedMediaPlayer {
	
	@Override
	public void playMp4(String fileName) {
		// do nothing		
	}
	
	@Override
	public void playVlc(String fileName) {
		System.out.println("Playing vlc file. Name: " + fileName);		
	}

	@Override
	public void playMp3(String fileName) {
		System.out.println("Playing mp3 file. Name: " + fileName);		
	}

}
