package fr.mds.designpattern.adapter.models;

public class Mp4Player implements AdvancedMediaPlayer {

	@Override
	public void playMp4(String fileName) {
		System.out.println("Playing mp4 file. Name: " + fileName);		
	}
	
	@Override
	public void playVlc(String fileName) {
		// do nothing		
	}

	@Override
	public void playMp3(String fileName) {
		System.out.println("Playing mp3 file. Name: " + fileName);			
	}
}
