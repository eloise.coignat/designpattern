package fr.mds.designpattern.adapter.models;

public class AudioPlayer implements MediaPlayer {
	
	MediaAdapter mediaAdapter;
	
	@Override
	public void play(String fileType, String fileName) {
		
		if (fileType.equalsIgnoreCase("mp4") || fileType.equalsIgnoreCase("vlc") || fileType.equalsIgnoreCase("mp3")) {
			mediaAdapter = new MediaAdapter(fileType);
			mediaAdapter.play(fileType, fileName);
		}
		else {
			System.out.println("Invalid audio type : " + fileType);
		}
	}
}
