package fr.mds.designpattern.adapter.models;

public interface MediaPlayer {
	
	public void play(String fileType, String fileName);
}
