package fr.mds.designpattern.abstractfactory.models.shape;

import fr.mds.designpattern.abstractfactory.models.AbstractFactory;
import fr.mds.designpattern.abstractfactory.models.color.Color;

public class ShapeFactory extends AbstractFactory {
	
	public static final String SHAPE = "SHAPE";
	
	@Override
	public Shape getShape(String choiceShape) {
		
		Shape shape;
		
		switch (choiceShape) {
			case Rectangle.RECTANGLE:
				shape = new Rectangle();
				break;
			case Circle.CIRCLE:
				shape = new Circle();
				break;
			case Square.SQUARE:
				shape = new Square();
				break;
			default:
				shape = null;
				break;
		}	
		return shape;				
	}

	@Override
	public Color getColor(String color) {
		return null;
	}

}
