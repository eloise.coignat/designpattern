package fr.mds.designpattern.abstractfactory.models.shape;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Circle implements Shape, Item {
	
	public static final String CIRCLE = "CIRCLE";
	
	public Circle() {

	}

	@Override
	public void draw() {
		System.out.println("This is a circle !");
	}

	@Override
	public String getName() {
		String name;		
		name = "Je m'appelle CIRCLE";		
		return name;
	}
	
	@Override
	public String toString() {
		return CIRCLE;
	}

}
