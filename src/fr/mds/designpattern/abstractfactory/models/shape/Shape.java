package fr.mds.designpattern.abstractfactory.models.shape;

public interface Shape {
	
	public void draw();
}
