package fr.mds.designpattern.abstractfactory.models.shape;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Square implements Shape, Item {
	
	public static final String SQUARE = "SQUARE";
	
	public Square() {
	
	}

	@Override
	public void draw() {
		System.out.println("This is a square !");
	}

	@Override
	public String getName() {		
		String name;		
		name = "Je m'appelle SQUARE";		
		return name;
	}
	
	@Override
	public String toString() {
		return SQUARE;
	}

}
