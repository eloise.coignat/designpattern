package fr.mds.designpattern.abstractfactory.models.shape;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Rectangle implements Shape, Item {
	
	public static final String RECTANGLE = "RECTANGLE";
	
	public Rectangle() {

	}

	@Override
	public void draw() {
		System.out.println("This is a rectangle !");
	}

	@Override
	public String getName() {
		String name;		
		name = "Je m'appelle RECTANGLE";		
		return name;
	}
	
	@Override
	public String toString() {
		return RECTANGLE;
	}

}
