package fr.mds.designpattern.abstractfactory.models;

import fr.mds.designpattern.abstractfactory.models.color.ColorFactory;
import fr.mds.designpattern.abstractfactory.models.shape.ShapeFactory;

public class FactoryProducer {
	
	public static AbstractFactory getFactory(String shapeOrColor) {
		
		AbstractFactory anAbstractFactory;
		
		switch (shapeOrColor) {
			case ColorFactory.COLOR:
				anAbstractFactory = new ColorFactory();
				break;
			case ShapeFactory.SHAPE:
				anAbstractFactory = new ShapeFactory();
				break;
			default:
				anAbstractFactory = null;
				break;
		}
		
		return anAbstractFactory;
	}
	
	public static Item getItem(String shapeOrColor) {		
		Item item;
		
		if ((Item) new ShapeFactory().getShape(shapeOrColor) != null) {
			item = (Item) new ShapeFactory().getShape(shapeOrColor);
		}
		else {
			item = (Item) new ColorFactory().getColor(shapeOrColor);
		}
		return item;
	}

}
