package fr.mds.designpattern.abstractfactory.models;

public interface Item {
	
	String getName();
}