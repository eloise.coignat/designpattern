package fr.mds.designpattern.abstractfactory.models;

import fr.mds.designpattern.abstractfactory.models.color.Color;
import fr.mds.designpattern.abstractfactory.models.shape.Shape;

public class CombinedItem {
	
	public Color aColor;
	public Shape aShape;
	
	public CombinedItem() {

	}
	
	public Color getColor() {
		return aColor;
	}

	public void setColor(Color aColor) {
		this.aColor = aColor;
	}

	public Shape getShape() {
		return aShape;
	}

	public void setShape(Shape aShape) {
		this.aShape = aShape;
	}

}
