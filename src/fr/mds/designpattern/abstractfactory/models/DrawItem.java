package fr.mds.designpattern.abstractfactory.models;

import java.util.*;

public class DrawItem {
	
	public String name;
	public List<CombinedItem> items = new ArrayList<CombinedItem>();
	
	public DrawItem() {

	}
	
	public void print() {
		for (CombinedItem item : items) {
			System.out.println(name + " : ");
			System.out.println("Shape --> " + item.getShape());
			System.out.println("Color --> " + item.getColor() + "\n");
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<CombinedItem> getItems() {
		return items;
	}

}
