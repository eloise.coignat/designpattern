package fr.mds.designpattern.abstractfactory.models;

import fr.mds.designpattern.abstractfactory.models.color.Color;
import fr.mds.designpattern.abstractfactory.models.shape.Shape;

public abstract class AbstractFactory {
	
	public abstract Shape getShape(String shape);
	
	public abstract Color getColor(String color);
}
