package fr.mds.designpattern.abstractfactory.models.color;

public interface Color {
	
	public void fill();
}
