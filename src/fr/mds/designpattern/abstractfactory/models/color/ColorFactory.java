package fr.mds.designpattern.abstractfactory.models.color;

import fr.mds.designpattern.abstractfactory.models.AbstractFactory;
import fr.mds.designpattern.abstractfactory.models.shape.Shape;

public class ColorFactory extends AbstractFactory {
	
	public static final String COLOR = "COLOR";
	
	@Override
	public Color getColor(String choiceColor) {
		
		Color color = null;
		
		switch (choiceColor) {
			case Blue.BLUE:
				color = new Blue();
				break;
			case Green.GREEN:
				color = new Green();
				break;
			case Red.RED:
				color = new Red();
				break;
			default:
				color = null;
				break;
		}	
		return color;	
	}

	@Override
	public Shape getShape(String shape) {
		return null;
	}

}
