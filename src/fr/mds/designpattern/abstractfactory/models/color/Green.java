package fr.mds.designpattern.abstractfactory.models.color;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Green implements Color, Item {
	
	public static final String GREEN = "GREEN";
	
	public Green() {

	}

	@Override
	public void fill() {
		System.out.println("I am GREEN !");
	}

	@Override
	public String getName() {
		String name;		
		name = "Je m'appelle GREEN";		
		return name;
	}
	
	@Override
	public String toString() {
		return GREEN;
	}

}
