package fr.mds.designpattern.abstractfactory.models.color;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Red implements Color, Item {
	
	public static final String RED = "RED";
	
	public Red() {
		
	}

	@Override
	public void fill() {
		System.out.println("I am RED !");
	}

	@Override
	public String getName() {
		String name;		
		name = "Je m'appelle RED";		
		return name;
	}
	
	@Override
	public String toString() {
		return RED;
	}

}
