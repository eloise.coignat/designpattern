package fr.mds.designpattern.abstractfactory.models.color;

import fr.mds.designpattern.abstractfactory.models.Item;

public class Blue implements Color, Item {
	
	public static final String BLUE = "BLUE";
	
	public Blue() {
		
	}

	@Override
	public void fill() {
		System.out.println("I am BLUE !");
	}

	@Override
	public String getName() {
		String name;		
		name = "Je m'appelle BLUE";		
		return name;
	}
	
	@Override
	public String toString() {
		return BLUE;
	}

}
