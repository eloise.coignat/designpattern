package fr.mds.designpattern.observer.launcher;

import java.util.ArrayList;
import java.util.List;

import fr.mds.designpattern.observer.models.Building;
import fr.mds.designpattern.observer.models.Classroom;
import fr.mds.designpattern.observer.models.Student;
import fr.mds.designpattern.observer.models.Teacher;

public class ObserverDemo {

    public static void main(String[] args) {
    	
    	List<Student> students1 = new ArrayList<Student>();
    	List<Student> students2 = new ArrayList<Student>();
    	List<Student> students3 = new ArrayList<Student>();
    	
    	Student jerem = new Student("Jerem");
        Student cedric = new Student("Cedric");
        Student eloise = new Student("Eloise");
        
        students1.add(jerem);
    	students1.add(cedric);
    	students1.add(eloise);
        
        Student mathieu = new Student("Mathieu");
        Student jerem1 = new Student("Jerem1");
        Student cedric1 = new Student("Cedric1");
        
        students2.add(mathieu);
        students2.add(cedric1);
        students2.add(jerem1);
        
        Student eloise1 = new Student("Eloise1");
        Student mathieu1 = new Student("Mathieu1");
        Student cedric2 = new Student("Cedric2");
        
        students3.add(eloise1);
        students3.add(mathieu1);
        students3.add(cedric2);
                
        Teacher micka = new Teacher("Micka");
        Teacher micka1 = new Teacher("Micka1");
        Teacher micka2 = new Teacher("Micka2");
        
        Classroom K12 = new Classroom("K12", students1,  micka);
        Classroom K13 = new Classroom("K13", students2,  micka1);
        Classroom K14 = new Classroom("K14", students3,  micka2);

        Building mds = new Building("MDS");
        
        mds.fire();
        
        mds.notifyEvent("Il y a le feu !!!!");
        
        K12.attach(micka);
        
        K12.notifyEvent("Sortez tous !!!!");

        micka.attach(jerem);
        micka.attach(cedric);
        micka.attach(eloise);
        
        micka.notifyEvent("ALED !!!!");

        

    }

}
