package fr.mds.designpattern.observer.models;

public interface Observer {

    void update(String msg);
}
