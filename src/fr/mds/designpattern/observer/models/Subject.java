package fr.mds.designpattern.observer.models;

public interface Subject {

    void attach(Observer observer);

    void detach(Observer observer);

    void notifyEvent(String msg);
}

