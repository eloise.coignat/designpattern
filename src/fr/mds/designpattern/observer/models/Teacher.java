package fr.mds.designpattern.observer.models;

import java.util.ArrayList;
import java.util.List;

public class Teacher implements Subject, Observer {

    private String name;

    private final List<Observer> observers = new ArrayList<Observer>();
    
    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public void attach(Observer observer) {
        if (!this.observers.contains(observer)) {
            this.observers.add(observer);
        }

    }

    @Override
    public void detach(Observer observer) {
        if (this.observers.contains(observer)) {
            this.observers.remove(observer);
        }
    }

    @Override
    public void notifyEvent(String msg) {
        for (Observer observer : observers) {
            observer.update(msg);
        }
    }

	@Override
	public void update(String msg) {
		System.out.println(this.name + " sort de la classe... " + msg);		
	}

}