package fr.mds.designpattern.observer.models;

import java.util.*;

public class Classroom implements Subject, Observer {
	
	private String name;
	private List<Student> students = new ArrayList<Student>();
	private final List<Observer> observers = new ArrayList<Observer>();
	private Teacher teacher;

	public Classroom(String name, List<Student> students, Teacher teacher) {
		this.name = name;
		this.students = students;
		this.teacher = teacher;
	}
	
	@Override
	 public void attach(Observer observer) {
	     if (!this.observers.contains(observer)) {
	            this.observers.add(observer);
	      }

	 }

	 @Override
	 public void detach(Observer observer) {
	     if (this.observers.contains(observer)) {
	            this.observers.remove(observer);
	       }
	  }

	 @Override
	 public void notifyEvent(String msg) {
	     for (Observer observer : observers) {
	            observer.update(msg);
	       }
	 }

	@Override
	public void update(String msg) {
		System.out.println(this.name + " est alert�e ");		
	}

}

