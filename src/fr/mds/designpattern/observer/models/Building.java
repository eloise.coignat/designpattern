package fr.mds.designpattern.observer.models;

import java.util.ArrayList;
import java.util.List;

public class Building implements Subject {
	
	private String name;
	
	private final List<Observer> observers = new ArrayList<Observer>();
	
	public Building(String name) {
		this.name = name;
	}
	
	public void fire() {
		System.out.println("ALERTE YA LE FEU !!!!!!");
	}

	 @Override
	 public void attach(Observer observer) {
	     if (!this.observers.contains(observer)) {
	            this.observers.add(observer);
	      }

	  }

	  @Override
	  public void detach(Observer observer) {
	      if (this.observers.contains(observer)) {
	            this.observers.remove(observer);
	       }
	   }

	   @Override
	   public void notifyEvent(String msg) {
	       for (Observer observer : observers) {
	            observer.update(msg);
	        }
	   }
}

